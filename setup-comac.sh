#!/bin/sh

set -x

if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

if [ -f $OURDIR/comac-done ]; then
    exit 0
fi

logtstart "comac"

cd $OURDIR

if [ -n ${COMAC_CHART_REPO} ]; then
    mkdir -p charts
    git clone ${COMAC_CHART_REPO} charts/helm-charts
    if [ -n "${COMAC_CHART_COMMIT}" ]; then
	cd charts/helm-charts \
	    && git checkout ${COMAC_CHART_COMMIT} \
	    && cd ../..
    fi
    cd charts/helm-charts \
        && helm dep update comac \
	&& helm upgrade --install comac comac \
	&& cd ../..
    cd charts/helm-charts/omec \
        && helm dep update omec-data-plane \
        && helm dep update omec-control-plane \
	&& helm upgrade --install --namespace omec omec-data-plane omec-data-plane \
	&& helm upgrade --install --namespace omec omec-control-plane omec-control-plane \
	&& cd ../../..
elif [ -n ${COMAC_VERSION} ]; then
    helm install -n comac cord/comac
    helm install --namespace omec -n omec-data-plane cord/omec-data-plane
    helm install --namespace omec -n omec-control-plane cord/omec-control-plane
fi

echo "Waiting for OMEC pods..."
while [ 1 ] ; do
    count=`kubectl get pods -n omec | grep -vE "Running|NAME" | wc -l`
    if [ -n "$count" -a $count -eq 0 ]; then
	echo "All OMEC pods running."
	break
    fi
    sleep 8
done

logtend "comac"
touch $OURDIR/comac-done
