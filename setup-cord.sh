#!/bin/sh

set -x

if [ -z "$EUID" ]; then
    EUID=`id -u`
fi
if [ $EUID -ne 0 ] ; then
    echo "This script must be run as root" 1>&2
    exit 1
fi

# Grab our libs
. "`dirname $0`/setup-lib.sh"

if [ -f $OURDIR/cord-done ]; then
    exit 0
fi

logtstart "cord"

cd $OURDIR

helm init --wait --client-only
helm repo add incubator \
    https://kubernetes-charts-incubator.storage.googleapis.com/
helm repo add cord https://charts.opencord.org
helm repo update

if [ -n ${CORD_PLATFORM_CHART_REPO} ]; then
    mkdir -p charts
    git clone ${CORD_PLATFORM_CHART_REPO} charts/cord-platform
    if [ -n "${CORD_PLATFORM_CHART_COMMIT}" ]; then
	cd charts/cord-platform \
	    && git checkout ${CORD_PLATFORM_CHART_COMMIT} \
	    && cd ..
    fi
    cd charts/cord-platform \
        && helm dep update cord-platform \
	&& helm upgrade --install cord-platform cord-platform \
	&& cd ..
else
    helm install -n cord-platform cord/cord-platform --version=${CORD_VERSION}
fi

# Wait until all pods are Running.
echo "Waiting for CORD platform pods..."
while [ 1 ] ; do
    count=`kubectl get pods | grep -vE "Running|NAME" | wc -l`
    if [ -n "$count" -a $count -eq 0 ]; then
	echo "All CORD platform pods running."
	break
    fi
    sleep 8
done

logtend "cord"
touch $OURDIR/cord-done
